<!DOCTYPE html>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<!-- Access the bootstrap Css like this, 
		Spring boot will handle the resource mapping automcatically -->
<link rel="stylesheet" type="text/css"
	href="webjars/bootstrap/3.3.7/css/bootstrap.min.css" />
<title>JscBook -- the AI-powered note taker on the web</title>
</head>
<body>
	<div class="jumbotron">
		<h1>Welcome to JscBook -- the AI-powered note taker on the web.</h1>
	</div>
	<div class="container-full">
		Just end your item with a newline, it will be classified
		automatically.
		<textarea readonly="readonly" id="iconId" rows="3" cols="1"></textarea>	
		<textarea class="form-control" id="exampleFormControlTextarea1"
			rows="3"></textarea>
	</div>
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="webjars/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script>
	<!--
$.ready(function() {
	$('#exampleFormControlTextarea1').change(function() {
		var text = $(this).text().split(/\r?\n/);
		$.each(text, function(i, v) {
			$.post("/ai", v, null, "text").done(function(data) {
								
			});});
});
	-->
	</script>
</body>