package us.d8u.balance.local;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PairRepository extends JpaRepository<Pair, Long> {
	@Query("SELECT f FROM Pair f WHERE f.entry = :entryKey")
	Optional<List<Pair>> findPairsForEntryOrderByTimestamp(@Param("entry")Integer entry);
}
