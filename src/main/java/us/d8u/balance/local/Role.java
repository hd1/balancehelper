package us.d8u.balance.local;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;

import com.opencsv.CSVWriter;
import com.opencsv.ICSVWriter;

@Entity
public class Role implements GrantedAuthority {
	/**
	 *
	 */
	private static final long serialVersionUID = 3998255686082715055L;

	private static Logger LOG = LoggerFactory.getLogger(Role.class);

	@Autowired
	private transient RoleRepository repo;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column
	private String name;

	@ManyToOne(fetch = FetchType.EAGER)
	private Users user;

	@ManyToOne
	@JoinColumn(name = "userId")
	public Users getUser() {

		return this.user;
	}

	public void setUser(Users users) {

		this.user = users;
	}

	public String getName() {

		return this.name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public Long getId() {

		return this.id;
	}

	public void setId(long studentId) {

		this.id = studentId;
	}

	public Role() {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(Application.getAiUrl().get(0),
					Application.getAiUrl().get(1),
					Application.getAiUrl().get(2));
			PreparedStatement prepped = conn.prepareStatement(
					"INSERT INTO "
							+ Role.class.getAnnotation(Table.class).name()+"(name, id) values(?, ?)",
							ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_UPDATABLE);
			prepped.setString(1, "admin");
			prepped.setInt(2, 1);
			prepped.executeQuery();
			prepped.setString(1, "user");
			prepped.setInt(2, 2);
			prepped.executeQuery();
		} catch (Exception e) {
			Role.LOG.error(e.getMessage(), e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e1) {
					Role.LOG.error(e1.getMessage(), e1);
				}
			}
		}

	}

	@Override
	public String getAuthority() {

		List<Role> csvOfRoles = this.repo.findAll();
		StringWriter retSrc = new StringWriter();
		CSVWriter csvWriter = new CSVWriter(retSrc,
				ICSVWriter.DEFAULT_SEPARATOR, ICSVWriter.NO_QUOTE_CHARACTER,
				ICSVWriter.DEFAULT_ESCAPE_CHARACTER,
				ICSVWriter.DEFAULT_LINE_END);
		for (Role r : csvOfRoles) {
			csvWriter
			.writeNext((String[]) Arrays.asList(r.getName()).toArray());
		}
		try {
			csvWriter.close();
		} catch (IOException e) {
			Role.LOG.error(e.getMessage(), e);
		}
		return retSrc.toString();

	}
}
