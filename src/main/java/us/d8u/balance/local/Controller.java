package us.d8u.balance.local;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@Configuration
@PropertySource("classpath:application.properties")
@RestController
public class Controller implements Filter {

	private static List<Map<String, Long>> sessions = new ArrayList<>();

	private static Logger LOG = LoggerFactory.getLogger(Controller.class);

	@Value("${pbf.path")
	String pbfPath;

	@Autowired
	private EntryRepository entryRepo;

	@Autowired
	private PairRepository pairRepo;

	@Autowired
	private UserRepository userRepo;

	static OkHttpClient client = new OkHttpClient();

	String jdbcUrl = Controller.getUrl();

	public static String getUrl() {

		Request request = new Request.Builder()
				.addHeader("Authorization",
						"Bearer 91b762c7-02db-43aa-8b40-3b8e24d8255c")
				.addHeader("Accept", "application/vnd.heroku+json; version=3")
				.addHeader("Content-Type", "application/json")
				.url("https://api.heroku.com/addons/postgresql-vertical-41820/config")
				.build();
		String json = null;
		try {
			json = Controller.client.newCall(request).execute().body().string();
		} catch (IOException e2) {
			Controller.LOG.error(e2.getMessage(), e2);
		}
		String url = null;
		try {
			url = new ObjectMapper().readTree(json).get(0).get("value").asText();
		} catch (IOException e1) {
			Controller.LOG.error(e1.getMessage(), e1);
		}
		URI dbUri = null;
		try {
			dbUri = new URI(url);
		} catch (URISyntaxException e) {
			Controller.LOG.error(e.getMessage(), e);
		}
		String username = dbUri.getUserInfo().split(":")[0];
		String password = dbUri.getUserInfo().split(":")[1];
		String ret = "jdbc:postgresql://" + username + ":" + password + "@"
				+ dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath()+"?sslmode=require";
		Controller.LOG.info("database url is " + ret);
		return ret;
	}

	private String ngrok_host = "0.tcp.ngrok.io:16513";

	@RequestMapping(value = "/config", method = RequestMethod.GET)
	public ResponseEntity<Map<String, String>> config(
			@RequestParam Map<String, String> params) {

		String host = params.get("ngrok");
		if (host != null) {
			this.ngrok_host = host;
			return new ResponseEntity<>(
					Collections.singletonMap("ngrok", this.ngrok_host),
					HttpStatus.CREATED);
		}
		return new ResponseEntity<>(
				Collections.singletonMap("error",
						"missing parameter -- valid ones are \"ngrok\""),
				HttpStatus.BAD_REQUEST);
	}

	@RequestMapping(value = "/entries", method = RequestMethod.GET)
	public ResponseEntity<List<Entry>> getEntriesForUser(
			@RequestParam(name = "user", defaultValue = "-1") String userId) {

		Controller.LOG.info("/entries");
		List<Entry> ret = new LinkedList<>();
		List<Map<Entry, Long>> all = this.entryRepo.findAllOrderedByTimestamp();
		if (all.isEmpty()) {
			return new ResponseEntity<>(Collections.emptyList(),
					HttpStatus.NOT_FOUND);
		}
		Integer user = Integer.valueOf(userId);
		for (Map<Entry, Long> entry : all) {
			if (user != -1) {
				if (((Entry) Arrays.asList(entry.keySet()).get(1))
						.getOwner() != user) {
					all.remove(entry.keySet().toArray()[0]);
				}
			}
			Entry thisEntry = (Entry) entry.keySet().toArray()[0];
			ret.add(thisEntry);
		}

		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	@RequestMapping(value = "/entry", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<List<Map<String, String>>> ai(
			@RequestParam("entry") Long entry,
			@org.springframework.web.bind.annotation.RequestBody String text,
			HttpServletResponse servletResponse) {

		text = text.replaceAll("entry=[0-9]+&", "");
		text = text.replace("%5Cn", "\n");
		text = text.replace("+", " ");
		text = text.replace("%3A", ":");
		text = text.replace("%2F", "/");
		text = text.replace("%40", "@");
		text = text.replace("%2C", ",");
		text = text.replace("=", "");

		List<Map<String, String>> ret = new ArrayList<>();
		// differentiate between people and organizations
		ret.add(Collections.singletonMap("title", text.split("\n")[0]));
		List<String> lines = Arrays.asList(text.split("\n")).subList(1,
				text.split("\n").length);
		for (String line : lines) {
			if (line.startsWith("entry=")) {
				continue;
			}
			if (line == "") {
				continue;
			}

			ret.add(this.aiHelper(line, entry));
		}
		servletResponse.setContentType("application/json");
		servletResponse.setCharacterEncoding("UTF-8");
		return new ResponseEntity<>(ret, HttpStatus.OK);
	}

	private Map<String, String> aiHelper(String text, Long entry) {

		Map<String, String> ret = new HashMap<>();

		for (String line : text.split("\\n")) {
			if (line.startsWith("entry")) {
				continue;
			}
			if (line.equals("")) {
				continue;
			}

			Pair pair = new Pair();
			pair.setEntry(entry);
			pair.setValue(line);
			try {

				if (line.matches("(?:[0-9] ?){6,14}[+0-9]$")
						|| line.matches("[0-9]{1,3}\\.[0-9]{4,14}(?:x.+)?")
						|| (Long.parseLong(line) < 1e14)) {
					pair.setKey("phone");
					ret.put("phone", line);
					continue;
				}
			} catch (NumberFormatException e) {
			}
			// URLs
			try {
				URL u = new URL(line);
				if (u.getProtocol().startsWith("http")) {
					pair.setKey("webpage");
					this.pairRepo.save(pair);
					ret.put("webpage", line);
					continue;
				}
			} catch (MalformedURLException e) {
			}

			// email addresses -- somehow javamail sees phone numbers as email
			// addresses -- wtf?
			try {
				InternetAddress[] addresses = InternetAddress.parse(line);
				if (addresses != null) {
					pair.setKey("email");
					this.pairRepo.save(pair);
					ret.put("email", line);
					continue;
				}
			} catch (AddressException e1) {
			}

			// Dates
			MediaType JSON = MediaType.parse("text/plain");

			RequestBody body = RequestBody.create(JSON, line);
			Request request = new Request.Builder()
					.url("http://hasan.d8u.us:3001/").post(body).build();
			Response response = null;
			try {
				response = Controller.client.newCall(request).execute();
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			String resp = null;
			try {
				resp = response.body().string();
			} catch (IOException e1) {
				Controller.LOG.error(e1.getMessage(), e1);
			}
			Controller.LOG.info("Response for dates: " + resp);
			Map<String, String> responseMap = null;
			try {
				responseMap = new ObjectMapper().readValue(resp,
						new TypeReference<Map<String, String>>() {
				});
			} catch (JsonParseException e) {
				Controller.LOG.error(e.getMessage(), e);
			} catch (JsonMappingException e) {
				Controller.LOG.error(e.getMessage(), e);
			} catch (IOException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			if (responseMap.get("parsed") != null) {
				pair.setKey("date");
				ret.put("date", line);
				continue;
			}
			// nominatim for addresses

			/*
			 * request = new Request.Builder()
			 * .url(new HttpUrl.Builder()
			 * .addQueryParameter("format", "json")
			 * .addQueryParameter("q", line)
			 * .host("nominatim.openstreetmap.org").scheme("https")
			 * .addPathSegment("search").build())
			 * .addHeader("User-Agent", "IgJkrMTEcsLXgkJ").build();
			 * try {
			 * response = client.newCall(request).execute();
			 * } catch (IOException e) {
			 * Controller.LOG.error(e.getMessage(), e);
			 * }
			 * try {
			 * resp = response.body().string();
			 * } catch (IOException e1) {
			 * Controller.LOG.error(e1.getMessage(), e1);
			 * }
			 * NominatimList responseAsJson = null;
			 * try {
			 * responseAsJson = new ObjectMapper().readValue(resp,
			 * NominatimList.class);
			 * } catch (JsonParseException e1) {
			 * Controller.LOG.error(e1.getMessage(), e1);
			 * } catch (JsonMappingException e1) {
			 * Controller.LOG.error(e1.getMessage(), e1);
			 * } catch (IOException e1) {
			 * Controller.LOG.error(e1.getMessage(), e1);
			 * }
			 * if (responseAsJson.isEmpty() == false) {
			 */

			try {
				Connection conn = DriverManager.getConnection(
						"jdbc:postgresql://" + this.ngrok_host + "/nominatim?");
				PreparedStatement prepped = conn.prepareStatement(
						"SELECT count(id) from planet_osm_ppint WHERE name *~ ?",
						ResultSet.FETCH_UNKNOWN, ResultSet.CONCUR_READ_ONLY);
				prepped.setString(1, line);
				if (prepped.executeQuery().getInt(1) > 0) {
					pair.setKey("address");
				}
			} catch (SQLException e) {
				Controller.LOG.error(e.getMessage(), e);
			}
			if (pair.getKey() == null) {
				pair.setKey("unknown");
			}
			ret.put(pair.getKey(), line);
			this.pairRepo.save(pair);
		}

		return ret;
	}

	@RequestMapping(value = "/entry", method = RequestMethod.PATCH)
	public ResponseEntity<Pair> fix(
			@RequestParam(name = "entry") Integer entryId,
			@org.springframework.web.bind.annotation.RequestBody Map<String, Map<String, String>> line) {

		ResponseEntity<Pair> ret = null;

		List<Pair> entry = this.pairRepo
				.findPairsForEntryOrderByTimestamp(entryId).get();
		for (Pair editedPair : entry) {
			if (editedPair.getValue().equals(line.get("old").get("value"))) {
				editedPair.setValue(line.get("new").get("value"));
				this.pairRepo.saveAndFlush(editedPair);
				ret = new ResponseEntity<>(editedPair, HttpStatus.NO_CONTENT);
				return ret;
			}
		}
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/entry", method = RequestMethod.GET)
	public ResponseEntity<List<Entry>> entries(
			@RequestParam("userId") Integer user) {

		List<Entry> entries = this.entryRepo.findAll();
		for (Entry e : entries) {
			if (!e.getOwner().equals(user)) {
				entries.remove(e);
			}
		}
		HttpStatus status = null;
		if (entries.isEmpty()) {
			status = HttpStatus.NO_CONTENT;
		} else {
			status = HttpStatus.OK;
		}
		return new ResponseEntity<>(entries, status);
	}

	@RequestMapping(value = "/user", method = RequestMethod.POST)
	public ResponseEntity<Map<String, String>> createUser(
			@RequestParam Map<String, String> userInfo) {

		boolean isApi = !(userInfo.containsKey("password")
				&& userInfo.containsKey("login"));
		final Map<String, String> ret = new HashMap<>();
		try {
			if (!userInfo.containsKey("login")) {
				userInfo.put("login", Long.toHexString(
						SecureRandom.getInstanceStrong().nextLong()));
				ret.put("accessKey", userInfo.get("login"));
			}
			if (!userInfo.containsKey("password")) {
				userInfo.put("password", Long.toHexString(
						SecureRandom.getInstanceStrong().nextLong()));
				ret.put("accessSecret", userInfo.get("password"));
			}
		} catch (NoSuchAlgorithmException e) {
			Controller.LOG.error(e.getMessage(), e);
		}
		Users u = new Users();
		u.setLogin(userInfo.get("login"));

		u.setPassword(userInfo.get("password"));

		u.setBalance(new BigDecimal(10));
		this.userRepo.save(u);
		Connection arg0 = null;
		try {
			if (isApi) {

				arg0 = DriverManager.getConnection(Controller.getUrl());
				PreparedStatement stmt = arg0.prepareStatement(
						"update users set user_id = (user_id - 1) * -1  where user_id = ?",
						ResultSet.FETCH_FORWARD, ResultSet.CONCUR_UPDATABLE);
				stmt.setLong(1, u.getUserId());
				if (stmt.executeUpdate() != 1) {
					Controller.LOG.error("wrong number of rows updated");
				}

			}
		} catch (SQLException e) {
			throw new RuntimeException(e.getMessage());
		} finally {
			if (arg0 != null) {
				try {
					arg0.close();
				} catch (SQLException e) {
					Controller.LOG.error(e.getMessage(), e);
				}
			}
		}
		if (u.getUserId() != null) {
			ret.put("status", "created");
			return new ResponseEntity<>(ret, HttpStatus.CREATED);
		} else {
			ret.put("status", "failed");
			return new ResponseEntity<>(ret, HttpStatus.EXPECTATION_FAILED);
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		if (req.getRequestURI().equals("/user")
				&& (req.getMethod().equals("POST"))) {
			chain.doFilter(request, response);
		}
		if (req.getRequestURI().equals("/entries")) {
			chain.doFilter(request, response);
		}
		String authHeader = ((HttpServletRequest) (request))
				.getHeader("Authorization");

		if (authHeader != null) {
			StringTokenizer st = new StringTokenizer(authHeader);
			if (st.hasMoreTokens()) {
				String basic = st.nextToken();

				if (basic.equalsIgnoreCase("Basic")) {
					String credentials = new String(
							Base64.decodeBase64(st.nextToken()), "UTF-8")
							.trim();
					int p = credentials.indexOf(":");
					if (p != -1) {
						String login = credentials.substring(0, p).trim();
						String password = credentials.substring(p + 1).trim();
						if (this.userRepo.findOneByLoginAndPassword(login,
								password) != null) {
							for (Map<String, Long> isExpired : Controller.sessions) {
								Long expirationTime = isExpired
										.get(new ArrayList<>(isExpired.keySet())
												.get(1));
								if (expirationTime < System
										.currentTimeMillis()) {
									Controller.sessions.remove(isExpired);
								}
							}
							Long expirationTime = LocalDateTime.now()
									.plusMinutes(2L)
									.toEpochSecond(ZoneOffset.UTC);
							Map<String, Long> session = Collections
									.singletonMap(login, expirationTime);
							Controller.sessions.add(session);
							chain.doFilter(request, response);
						}
					}
				}
			}
		}
	}
}