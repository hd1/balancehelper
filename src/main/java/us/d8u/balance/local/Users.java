package us.d8u.balance.local;

import java.math.BigDecimal;

/**
 * Authentication keys and tokens
 */

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@Entity
public class Users implements UserDetails {
	/**
	 *
	 */
	private static final long serialVersionUID = -5054693029593413030L;

	private static Logger LOG = LoggerFactory.getLogger(Users.class);

	private static Users ret = new Users();

	public static UserDetails withUsername(String string) {

		Users.ret.setLogin(string);
		return Users.ret;
	}

	public static UserDetails password(String string) {

		Users.ret.setPassword(string);
		return Users.ret;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long userId;

	@Column(name = "username")
	String login;

	@Column(name = "password")
	String password;

	@Column
	Boolean enabled;

	@Column
	BigDecimal balance = new BigDecimal(10);

	@Column
	Date creationTime = new Date();

	@OneToMany(targetEntity = Role.class, mappedBy = "user", fetch = FetchType.EAGER)
	List<GrantedAuthority> roles;

	public Users() {

		SecureRandom secureRandom = null;
		try {
			secureRandom = SecureRandom.getInstanceStrong();
		} catch (NoSuchAlgorithmException e) {
			Users.LOG.error(e.getMessage(), e);
		}
		this.setLogin(Long.toHexString(secureRandom.nextLong()));
		this.setPassword(Long.toHexString(secureRandom.nextLong()));

	}

	public Long getUserId() {

		return this.userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public String getLogin() {

		return this.login;
	}

	public void setLogin(String login) {

		this.login = login;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public void setBalance(BigDecimal bigDecimal) {

		this.balance = bigDecimal;
	}

	public BigDecimal getBalance() {

		return this.balance;
	}

	public void setAuthorities(List<GrantedAuthority> authorities) {

		this.roles = authorities;
	}

	@Override
	public String getPassword() {

		return this.password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {

		List<Role> ret = new ArrayList<>();
		try {
			Connection conn = DriverManager
					.getConnection("jdbc:postgresql://ec2-107-22-160-185.compute-1.amazonaws.com:5432/dcpu8o0ssv2uqd?sslmode=require", "edfvectxozmkdk", "e1eede4ff6127969c7b717e710b7261f13c326c33dd0c23557657afa634dae49");
			ResultSet rs = conn
					.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY)
					.executeQuery("select name from roles");
			while (!rs.isLast()) {
				rs.next();
				Role r = new Role();
				r.setName(rs.getString(1));
				ret.add(r);
			}
		} catch (Exception e) {
			Users.LOG.error(e.getMessage(), e);
		}
		return ret;
	}

	public void setAuthorities(Collection<Role> auth) {

		try {
			Connection conn = DriverManager.getConnection(
					Application.getAiUrl().get(0),
					Application.getAiUrl().get(1),
					Application.getAiUrl().get(2));
			conn.setAutoCommit(false);
			PreparedStatement prepped = conn.prepareStatement(
					"INSERT INTO role(name) VALUES (?)",
					ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_UPDATABLE);

			for (Role r : auth) {
				prepped.setString(1, r.getName());
				prepped.executeQuery();
			}
			conn.commit();
		} catch (SQLException e) {
			Users.LOG.error(e.getMessage(), e);
		}
	}

	@Override
	public String getUsername() {

		return this.login;
	}

	@Override
	public boolean isAccountNonExpired() {

		return false;
	}

	@Override
	public boolean isAccountNonLocked() {

		return this.isEnabled();
	}

	@Override
	public boolean isCredentialsNonExpired() {

		return (this.isEnabled());
	}

	@Override
	public boolean isEnabled() {

		return this.getBalance().doubleValue() > 0.00;
	}

	public Boolean getEnabled() {

		return this.enabled;
	}

	public void setEnabled(Boolean enabled) {

		this.enabled = this.getBalance().doubleValue() > 0.00;
	}

	public Date getCreationTime() {

		return this.creationTime;
	}

	public void setCreationTime(Date creationTime) {

		this.creationTime = creationTime;
	}

	public List<GrantedAuthority> getRoles() {

		return this.roles;
	}

	public void setRoles(List<GrantedAuthority> roles) {

		this.roles = roles;
	}

}
