package us.d8u.balance.local;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class Application implements CommandLineRunner {

	private static Logger LOG = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}

	@Autowired
	UserRepository userRepo;

	public static List<String> getAiUrl() {

		List<String> ret = new ArrayList<>();

		URI dbUri = null;
		try {
			dbUri = new URI(
					"postgres://lnsvjkkjxnghwj:76a759c6820126da4cfb12f43e1e73525a826b898ee5ecb89fca2cc3d93dcbb9@ec2-107-21-120-104.compute-1.amazonaws.com:5432/de1u7ktfo20794");
		} catch (URISyntaxException e) {
			Application.LOG.error(e.getMessage(), e);
		}

		String username = dbUri.getUserInfo().split(":")[0];
		String password = dbUri.getUserInfo().split(":")[1];
		String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':'
				+ dbUri.getPort() + dbUri.getPath()
				+ "?ssl=true&sslmode=prefer&sslfactory=org.postgresql.ssl.NonValidatingFactory";
		ret.add(dbUrl);
		ret.add(username);
		ret.add(password);
		return ret;
	}

	@Override
	public void run(String... args) throws Exception {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(
					"jdbc:postgresql://ec2-107-21-120-104.compute-1.amazonaws.com:5432/de1u7ktfo20794?sslmode=require",
					"lnsvjkkjxnghwj",
					"76a759c6820126da4cfb12f43e1e73525a826b898ee5ecb89fca2cc3d93dcbb9");

			ResultSet rs = conn
					.createStatement(ResultSet.TYPE_FORWARD_ONLY,
							ResultSet.CONCUR_READ_ONLY)
					.executeQuery("SELECT id from users");
			while (rs.next()) {
				Users newest = new Users();
				newest.setUserId(Integer.toUnsignedLong(rs.getInt(1)));
				this.userRepo.save(newest);
			}
		} catch (SQLException e) {
			Application.LOG.error(e.getMessage(), e);
		} catch (Exception e) {
			Application.LOG.error("something severe may have wrong", e);
			return;
		} finally {
			if (conn != null) {
				conn.close();
			}
		}
		Application.LOG.info("imported users!");
	}

}
