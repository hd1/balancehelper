package us.d8u.balance.local;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EntryRepository extends JpaRepository<Entry, Long>, EntryRepositoryCustom {
	Optional<List<Entry>> findByOwner(Integer u);

	@Override
	List<Map<Entry, Long>> findAllOrderedByTimestamp();
}
