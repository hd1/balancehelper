package us.d8u.balance.local;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private static final Logger LOG = LoggerFactory
			.getLogger(WebSecurityConfig.class);

	@Autowired
	private DataSource dataSource;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth)
			throws Exception {

		Users superUser = new Users();
		superUser.setLogin("admin");
		auth.jdbcAuthentication().dataSource(this.dataSource)
		.withUser(superUser).dataSource(this.dataSource)
		.usersByUsernameQuery(
				"SELECT username, password, enabled from users where username=?")
		.authoritiesByUsernameQuery("select name from roles")
		.groupAuthoritiesByUsername("select r.name from roles r join users u on r.user = ?");
		WebSecurityConfig.LOG
		.error("super user -- login \"admin\" --'s password is "
				+ superUser.getPassword()
				+ "; it will only be displayed once -- so write it down!");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {

		return new BCryptPasswordEncoder();
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {

		httpSecurity.authorizeRequests().antMatchers("/entry")
		.authenticated().and().formLogin();
		httpSecurity.csrf().ignoringAntMatchers("/user");
		httpSecurity.csrf().ignoringAntMatchers("/entries");
		httpSecurity.headers().frameOptions().sameOrigin();
	}
}
