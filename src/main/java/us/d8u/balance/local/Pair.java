package us.d8u.balance.local;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class Pair implements Comparable<Pair> {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	Long pairId;

	@NotNull
	@Column
	String key;

	@NotNull
	@Column
	String value;

	@Column
	@NotNull
	Long entry;

	@Column
	@NotNull
	LocalDateTime timestamp;


	public Pair() {

		this.timestamp = LocalDateTime.now();
	}

	public Long getPairId() {

		return this.pairId;
	}

	public void setPairId(Long pairId) {

		this.pairId = pairId;
	}

	public String getKey() {

		return this.key;
	}

	public void setKey(String key) {

		this.key = key;
	}

	public String getValue() {

		return this.value;
	}

	public void setValue(String value) {

		this.value = value;
	}

	public boolean isKey() {

		return this.key.equals("key");
	}

	@Override
	public boolean equals(Object obj) {

		Pair other = (Pair) obj;
		return this.getKey().equals(other.getKey())
				&& this.getValue().equals(other.getValue());
	}

	@Override
	public int hashCode() {

		return (this.getKey().hashCode() * this.getValue().hashCode())
				% Integer.MAX_VALUE;
	}

	public Long getEntry() {

		return this.entry;
	}

	public void setEntry(Long entryId) {

		this.entry = entryId;
	}

	@Override
	public int compareTo(Pair o) {

		return this.timestamp.compareTo(o.timestamp);
	}

	public LocalDateTime getTimestamp() {

		return this.timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {

		this.timestamp = timestamp;
	}


}
