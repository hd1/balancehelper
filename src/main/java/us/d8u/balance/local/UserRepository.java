package us.d8u.balance.local;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Long> {

	Users findOneByLoginAndPassword(String login, String password);

}
