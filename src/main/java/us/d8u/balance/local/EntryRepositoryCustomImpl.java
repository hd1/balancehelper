package us.d8u.balance.local;

import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

@Repository
public class EntryRepositoryCustomImpl implements EntryRepositoryCustom {
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Entry> findByTitle(String title) {

		CriteriaBuilder cb = this.entityManager.getCriteriaBuilder();
		CriteriaQuery<Entry> query = cb.createQuery(Entry.class);
		Root<Entry> root = query.from(Entry.class);
		List<Entry> entries = new ArrayList<>();
		for (Entry e : this.entityManager.createQuery(query.select(root))
				.getResultList()) {
			if (e.getTitle().contains(title)) {
				entries.add(e);
				continue;
			}
			for (Pair p : e.getLines()) {
				if (p.getValue().contains(title)) {
					entries.add(e);
					break;
				}
			}
		}
		return entries;
	}

	@Override
	public List<Map<Entry, Long>> findAllOrderedByTimestamp() {

		List<Map<Entry, Long>> ret = new ArrayList<>();
		@SuppressWarnings("unchecked")
		List<Entry> allEntries = this.entityManager.createQuery("SELECT e FROM Entry e").getResultList();

		for (Entry entry : allEntries) {
			Long minimumTime = Long.MAX_VALUE;
			List<Pair> lines = entry.getLines();
			for (Pair line : lines) {
				if (line.getTimestamp()
						.toEpochSecond(ZoneOffset.UTC) < minimumTime) {
					minimumTime = line.getTimestamp()
							.toEpochSecond(ZoneOffset.UTC) * 1000L;
				}
			}
			ret.add(Collections.singletonMap(entry, minimumTime));
		}
		return ret;
	}
}