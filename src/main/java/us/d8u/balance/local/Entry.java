package us.d8u.balance.local;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Entry {
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private Long entryId;

	@Column
	private Integer owner;

	@Column
	private String title;

	@OneToMany
	private List<Pair> lines;

	public Integer getOwner() {

		return this.owner;
	}

	public void setOwner(Integer owner) {

		this.owner = owner;
	}

	public List<Pair> getLines() {

		return this.lines;
	}

	public void setLines(List<Pair> lines) {

		this.lines = lines;
	}

	public String getTitle() {

		return this.title;
	}

	public void setTitle(String title) {
		Pair p = new Pair();
		p.setKey("title");
		p.setValue(title);
		p.setEntry(this.entryId);
		this.lines.add(1, p);
		this.title = this.lines.get(1).getValue();
	}
}
