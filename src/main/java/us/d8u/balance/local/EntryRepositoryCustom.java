package us.d8u.balance.local;

import java.util.List;
import java.util.Map;

public interface EntryRepositoryCustom {
	List<Map<Entry,Long>> findAllOrderedByTimestamp();
	List<Entry> findByTitle(String title);
}
