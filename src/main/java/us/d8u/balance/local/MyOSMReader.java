package us.d8u.balance.local;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.container.v0_6.WayContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;
import org.openstreetmap.osmosis.core.domain.v0_6.Way;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crosby.binary.osmosis.OsmosisReader;

public class MyOSMReader implements Sink, Runnable {
	private static MyOSMReader instance;

	public static MyOSMReader getInstance() {

		if (MyOSMReader.instance == null) {
			MyOSMReader.instance = new MyOSMReader();
			MyOSMReader.instance.run();
		}
		return MyOSMReader.instance;

	}

	private String pbfPath;

	private static Logger LOG = LoggerFactory.getLogger(MyOSMReader.class);

	Set<Way> interesting;

	@Override
	public void initialize(Map<String, Object> arg0) {

		try {
			this.pbfPath = Class.forName("MyOSMReader").getResource("world.pbf")
					.toExternalForm();
		} catch (ClassNotFoundException e) {
			MyOSMReader.LOG.error(e.getMessage(), e);
		}
		this.interesting = new HashSet<>();
	}

	@Override
	public void complete() {

	}

	@Override
	public void close() {

	}

	@Override
	public void process(EntityContainer arg0) {

		if (arg0 instanceof WayContainer) {
			Way myWay = ((WayContainer) arg0).getEntity();
			myWay.getTags().stream().forEach(t -> MyOSMReader.LOG
					.info(t.getKey() + " => " + t.getValue()));
			for (Tag myTag : myWay.getTags()) {
				String key = myTag.getKey();
				if (key.startsWith("address")) {
					this.interesting.add(myWay);
				}
			}
		}

	}

	@Override
	public void run() {

		MyOSMReader ret = new MyOSMReader();
		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream(this.pbfPath);
		} catch (FileNotFoundException e) {
			MyOSMReader.LOG.error(e.getMessage(), e);
		}
		OsmosisReader reader = new OsmosisReader(inputStream);
		reader.setSink(ret);
		reader.run();
	}

	public boolean contains(String address) {

		for (Way w : this.interesting) {
			List<Tag> tags = new ArrayList<>();
			tags.addAll(w.getTags());
			String addressLine = "";
			for (Tag tag : tags) {
				if (tag.getKey().startsWith("addr")) {
					addressLine += tag.getValue().contains(address) + " ";
				}
			}
			if (addressLine.contains(address)) { return true; }
		}
		return false;
	}

}
