package us.d8u.balance.local;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class EndpointsTest {
	private static Logger LOG = LoggerFactory.getLogger(EndpointsTest.class);

	@Autowired
	private TestRestTemplate mockMvc;

	@LocalServerPort
	Integer port;

	final String BODY = "Hasan Diwan\nhasan.diwan@gmail.com\n+14156907930\nhttp://hasan.d8u.us\nDecember 4, 1979\n";

	final String BODY2 = "Jackson St. Capitol\nManaging Director: Jaan van der Plonk\nDirector of Analytics: Hasan Diwan\nDirector of Sales: Jawad Ansari\nUS Address: 100 Shoreline Hwy, Mill Valley, CA 94941\nEU Address: Carrer d'en Roca 9, 08002 Barcelona, Spain\nUK Address: 105 Philbeach Gardens, Earl's Court, London SW5 9ET, UK\nUS Phone Number: 1-617-431-6334‬\nNon-US Phone Number: +44 79070 122 467\n";

	private ResponseEntity<Map<String, List<String>>> getResponse() {

		URI uri = null;
		try {
			uri = new URI("http://localhost:" + this.port + "/ai");
		} catch (URISyntaxException e) {
			EndpointsTest.LOG.error(e.getMessage(), e);
		}
		ResponseEntity<Map<String, List<String>>> resp_ = this.mockMvc.exchange(
				uri, HttpMethod.POST, null,
				new ParameterizedTypeReference<Map<String, List<String>>>() {
				});
		EndpointsTest.LOG.error("*** response from " + uri.toString() + " is "
				+ resp_.getBody());
		return resp_;
	}

	@Test
	public void aiParsesDateCorrectly() throws Exception {

		ResponseEntity<Map<String, List<String>>> resp_ = this.getResponse();
		Assert.assertTrue(resp_.getBody().containsKey("dates"));
		Assert.assertEquals(resp_.getBody().get("dates").get(1),
				"December 4, 1979");
	}

	@Test
	public void aiContainsKey() throws Exception {

		ResponseEntity<Map<String, List<String>>> resp_ = this.getResponse();
		Assert.assertTrue(resp_.getBody().containsKey("key"));
		Assert.assertEquals(resp_.getBody().get("key").get(1), "Hasan Diwan");
	}

	@Test
	public void aiContainsWebpages() throws Exception {

		ResponseEntity<Map<String, List<String>>> resp_ = this.getResponse();
		Assert.assertTrue(resp_.getBody().containsKey("webpages"));
		Assert.assertEquals(resp_.getBody().get("webpages").get(1),
				"http://hasan.d8u.us");
	}

	@Test
	public void aiContainsPhoneNumbers() throws Exception {

		ResponseEntity<Map<String, List<String>>> resp_ = this.getResponse();
		Assert.assertTrue(resp_.getBody().containsKey("phoneNumbers"));
		Assert.assertEquals(resp_.getBody().get("phoneNumbers").get(1),
				"+14156907930");
	}

	@Test
	public void aiContainsDates() {

		ResponseEntity<Map<String, List<String>>> resp_ = this.getResponse();
		Assert.assertTrue(resp_.getBody().containsKey("dates"));
		Assert.assertEquals(resp_.getBody().get("dates").get(1),
				"December 4, 1979");
	}

	@Test
	public void aiContainsEmail() throws Exception {

		ResponseEntity<Map<String, List<String>>> resp_ = this.getResponse();
		Assert.assertTrue(resp_.getBody().containsKey("email"));
		Assert.assertEquals(resp_.getBody().get("email").get(1),
				"hasan.diwan@gmail.com");
	}

	public void entriesReturnsNullIfNoEntries() throws Exception {

		ResponseEntity<List<Entry>> results = this.mockMvc.exchange(
				RequestEntity.get(new URI(
						"http://localhost:" + this.port + "/entries?user=0"))
				.build(),
				new ParameterizedTypeReference<List<Entry>>() {
				});
		Assert.assertEquals(results.getStatusCode(), HttpStatus.NOT_FOUND);
		Assert.assertTrue(results.getBody().isEmpty());
	}

	public void entriesReturnsContentIfEntriesExist() throws Exception {

		Pair p = new Pair();
		p.setEntry(1L);
		p.setKey("title");
		p.setValue("Hasan Diwan");
		Pair p2 = new Pair();
		p2.setEntry(1L);
		p2.setKey("birthday");
		p2.setValue("December 4, 1979");
		Assert.assertNotNull(this.mockMvc.getForObject("/entry", Map.class, 1));

	}
}
